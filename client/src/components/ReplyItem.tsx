import * as React from 'react';
import { calculateReactions } from '../helpers/calculateReactions';

export const ReplyItem = ({ id, text, reactions }: any) => {
  const [likes, dislikes] = calculateReactions(reactions);

  return (
    <div className="reply">
      <p className="reply__text">{text}</p>
      <small className="reply__reactions text-small">
        likes: {likes}, dislikes: {dislikes}
      </small>
      <small className="reply__id text-small">{id.slice(-6)}</small>
    </div>
  );
};

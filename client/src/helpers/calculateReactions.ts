export const calculateReactions = (reactions: Array<{ isLike: boolean }> = []) =>
  reactions.reduce(
    (acc: any, cur: any) => {
      if (cur) {
        acc[0]++;
      } else {
        acc[1]++;
      }
      return acc;
    },
    [0, 0]
  );

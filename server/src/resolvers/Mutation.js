async function postMessage(parent, args, context, info) {
  if (!args.messageId) {
    return context.prisma.createMessage({
      text: args.text
    });
  }
}

async function postReply(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist.`);
  }

  return context.prisma.createReply({
    text: args.text,
    message: { connect: { id: args.messageId } }
  });
}

async function reactToMessage(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist.`);
  }

  context.prisma.createMessageReaction({
    isLike: args.isLike,
    message: { connect: { id: args.messageId } }
  });

  return context.prisma.message({ id: args.messageId });
}

async function reactToReply(parent, args, context, info) {
  const replyExists = await context.prisma.$exists.reply({
    id: args.replyId
  });

  if (!replyExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist.`);
  }

  context.prisma.createReplyReaction({
    isLike: args.isLike,
    reply: { connect: { id: args.replyId } }
  });

  return context.prisma.reply({
    id: args.replyId
  });
}

module.exports = {
  postMessage,
  postReply,
  reactToMessage,
  reactToReply
};

import * as React from 'react';
import { gql, useMutation } from '@apollo/client';
import { calculateReactions } from '../helpers/calculateReactions';
import { MESSAGE_QUERY, POST_MESSAGE_REACTION_MUTATION } from '../queries/messages';
import { ReplyList } from './ReplyList';

export const MessageItem = ({ id, text, reactions, replies }: any) => {
  const orderBy = 'createdAt_DESC';

  const [likes, dislikes] = calculateReactions(reactions);
  const [addReaction] = useMutation(POST_MESSAGE_REACTION_MUTATION, {
    update(cache, result) {
      console.log(result);
    }
  });

  const _updateStoreAfterReacting = (store: any, newReaction: any, messageId: any) => {
    const orderBy = 'createdAt__DESC';
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy
      }
    });
    data.messages.messageList
      .find((item: any) => item.id === messageId)
      .reactions.unshift(newReaction);
    store.writeQuery({ query: MESSAGE_QUERY, data });
  };

  return (
    <div className="message">
      <p className="message__text">{text}</p>
      <small className="message__reactions text-small">
        <button
          onClick={e => {
            console.log(id);

            addReaction({ variables: { messageId: id, isLike: true } });
          }}
        >
          likes: {likes}
        </button>
        <button>dislikes: {dislikes}</button>
      </small>
      <ReplyList messageId={id} replies={replies} />
      <small className="message__id text-small">{id.slice(-6)}</small>
    </div>
  );
};

function newMessageSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .message({
      mutation_in: ['CREATED']
    })
    .node();
}

const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: payload => {
    return payload;
  }
};

function newReplySubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .reply({
      mutation_in: ['CREATED']
    })
    .node();
}

const newReply = {
  subscribe: newReplySubscribe,
  resolve: payload => {
    return payload;
  }
};

function newMessageReactionSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .messageReaction({
      mutation_in: ['CREATED']
    })
    .node();
}

const newMessageReaction = {
  subscribe: newMessageReactionSubscribe,
  resolve: payload => {
    return payload;
  }
};

function newReplyReactionSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .replyReaction({
      mutation_in: ['CREATED']
    })
    .node();
}

const newReplyReaction = {
  subscribe: newReplyReactionSubscribe,
  resolve: payload => {
    return payload;
  }
};

module.exports = {
  newMessage,
  newReply,
  newReplyReaction,
  newMessageReaction
};

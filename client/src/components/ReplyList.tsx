import * as React from 'react';
import { ReplyItem } from './ReplyItem';

export const ReplyList = ({ messageId, replies }: any) => (
  <div className="reply-list">{replies && replies.map((item: any) => <ReplyItem {...item} />)}</div>
);

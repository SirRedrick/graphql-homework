import * as React from 'react';
import { useQuery } from '@apollo/client';
import { MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION } from '../queries/messages';
import { MessageItem } from './MessageItem';

export const MessageList = () => {
  const orderBy = 'createdAt_DESC';
  const { data, loading, error } = useQuery(MESSAGE_QUERY, {
    variables: { filter: '', orderBy }
  });

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Fetch error</div>;

  const {
    messages: { messageList }
  } = data;

  return (
    <div className="message-list">
      {messageList.map((item: any) => {
        return <MessageItem key={item.id} {...item} />;
      })}
    </div>
  );
};

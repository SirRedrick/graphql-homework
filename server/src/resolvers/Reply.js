function message(parent, args, context) {
  return context.prisma
    .reply({
      id: parent.id
    })
    .message();
}

function reactions(parent, args, context) {
  return context.prisma
    .reply({
      id: parent.id
    })
    .reactions();
}

module.exports = {
  message,
  reactions
};

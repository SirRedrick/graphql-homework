function replies(parent, args, context) {
  return context.prisma
    .message({
      id: parent.id
    })
    .replies();
}

function reactions(parent, args, context) {
  return context.prisma
    .message({
      id: parent.id
    })
    .reactions();
}

module.exports = {
  replies,
  reactions
};

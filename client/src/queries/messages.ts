import { gql } from '@apollo/client';

export const MESSAGE_QUERY = gql`
  query messageQuery($filter: String, $orderBy: MessageOrderByInput) {
    messages(filter: $filter, orderBy: $orderBy) {
      count
      messageList {
        id
        text
        reactions {
          isLike
        }
        replies {
          id
          text
          reactions {
            isLike
          }
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation postMessage($text: String!) {
    postMessage(text: $text) {
      id
      text
      reactions {
        isLike
      }
      replies {
        id
        text
        message {
          id
        }
        reactions {
          isLike
        }
      }
    }
  }
`;

export const POST_MESSAGE_REACTION_MUTATION = gql`
  mutation postMessageReaction($messageId: ID!, $isLike: Boolean!) {
    reactToMessage(messageId: $messageId, isLike: $isLike) {
      id
      reactions {
        id
        isLike
      }
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      reactions {
        isLike
      }
      replies {
        id
        text
        reactions {
          isLike
        }
      }
    }
  }
`;

export const NEW_MESSAGE_REACTION_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      reactions {
        isLike
      }
      replies {
        id
        text
        reactions {
          isLike
        }
      }
    }
  }
`;

import gql from 'graphql-tag';

export const REPLIES_QUERY = gql`
  query messageQuery($filter: String, $orderBy: ProductOrderByInput) {
    messages(filter: $filter, orderBy: $orderBy) {
      count
      messageList {
        id
        text
        reactions {
          isLike
        }
        replies {
          id
          text
          reactions {
            isLike
          }
        }
      }
    }
  }
`;
